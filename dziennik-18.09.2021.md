# 18.09.2021

## Drugi dzień realizacji projektu (Sobota)

- Niestety dzisejszy dzień był dla mnie bardo intensywny i miałem niewiele czasu na realizowanie projektu.

- Postanowiłem że dziś zrobie sobie przerwę z okazji soboty, Jednak zmieniłem zdanie i zdecydowałem, że połącze komponenty elektroniczne w bardziej permanentny sposób. Przylutowałem z jednej strony końcówki do mikrokontrolera, a ze strony przeciwnej przylutowałem złącza żeńskie goldpin (raster 2,45).

- Wykonałem także szkic obudowy uwzględniając rzeczywiste rozmiary użytych części.
