#include <Wire.h>
#define DHT11PIN 4 //dht11
#include <dht11.h> //dht11
dht11 DHT11;
#include "i2c.h"
#include "i2c_BMP280.h"
BMP280 bmp280;
// Include NewLiquidCrystal Library for I2C
#include <LiquidCrystal_I2C.h>
// Define LCD pinout
const int  en = 2, rw = 1, rs = 0, d4 = 4, d5 = 5, d6 = 6, d7 = 7, bl = 3;
// Define I2C Address - change if reqiuired (lcd)
const int i2c_addr = 0x27;
LiquidCrystal_I2C lcd(i2c_addr, en, rw, rs, d4, d5, d6, d7, bl, POSITIVE);

void setup()
{
    Serial.begin(115200);

    Serial.print("Probe BMP280: ");
    if (bmp280.initialize()) Serial.println("Sensor found");
    else
    {
        Serial.println("Sensor missing");
        while (1) {}
    }
    // Set display type as 16 char, 2 rows
    lcd.begin(16,2);
    // Print on first row
    lcd.setCursor(4,0);
    lcd.print("Cyfrowy");
    lcd.setCursor(4,1);
    lcd.print("Termometr");
  
    // Wait 3 seconds
    delay(3000);
    lcd.clear();
  
    // Print on second row
    lcd.setCursor(7,0);
    lcd.print("I");
    lcd.setCursor(2,1);
    lcd.print("Cisnieniometr");
  
    // Wait 4seconds
    delay(4000);
  
    // Clear the display
    lcd.clear();

    // onetime-measure:
    bmp280.setEnabled(0);
    bmp280.triggerMeasurement();
}

void loop()
{
    
    bmp280.awaitMeasurement();

    float temperature;
    bmp280.getTemperature(temperature);

    float pascal;
    bmp280.getPressure(pascal);
    bmp280.triggerMeasurement();
    lcd.setCursor(0,0);
    lcd.print("C:");
    lcd.setCursor(4,0);
    lcd.print(temperature);
    lcd.print("*C ");


    lcd.setCursor(0,1);
    lcd.print("P: ");
    lcd.print(pascal/100);
    lcd.print("HPa");


    delay(1000);
}

/**<

Program size:
A1.0.5:
A1.5.7: 9680b
A1.6.3: 9664b / 561b

 */
