# 17.09.2021
## Pierwszy dzień realizacji projektu (Piątek)

 Postanowiłem dziś zacząć od napisania kodu dla mikrokontrolera.

 Wykorzystam gotowe przykłady bibliotek i dodam własny kod, który będzie służył do zebrania danych, sformatowania go oraz ich wyświetlenia.


- Napisałem podstawową wersję kodu lecz spostrzegłem, że wyświetlacz, który posiadam (lcd 16x2) nie ma odpowiednich rozmiarów do ilości danych jakie chcę na nim wyświetlić.
Posiadam jeszcze wyświetlacz (prawdopodobnie) 20x4 lecz nie korzystałem jeszcze nigdy z niego i chcę uniknąć dodatkowych utrudnień.

- Postanowiłem, że w związku z ograniczonym miejscem na wyświtlaczu wykorzystam jedynie jeden czujnik (BMP280). Niestety dzięki tej zmianie nie będę miał odczytu wilgotności. Jednak tym nie martwię się za bardzo, gdyż czujnik wilgotniości jaki chciałem użyć nie miał za precyzyjnych odczytów

- Przemyślałem pomysł użycia "czystej" atmegi 328p (lub atmegi 8) lecz uznałem, że obecność regultora napięcia 3.3V na gotowych płytkach arduino będzie sporym ułatwieniem.

- Zmontowałem oraz uruchomiłem projekt na płytce stykowej. Wprowadziłem także parę drobnych poprawek do kodu. Narazie wszystko działa jak należy.

- Jutro postaram się zaprojektować oraz wykonać obudowę do tego projektu.

